from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .models import Contenido #importo Contenido de models
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout


# Create your views here.

formulario = """
No existe valor en la base de datos para esta llave.
<p>Introducela:
<p>
<form action="" method = "POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
            respuesta = "El valor de la llave es: " + c.valor
            c.save()
        except Contenido.DoesNotExist:
            if request.user.is_authenticated:
                 c = Contenido(clave=llave, valor=valor)
                 respuesta = "La llave " + c.clave + " tiene como valor " + c.valor
                 c.save()
            else:
                respuesta = "No estas autenticado. <a href='/login'> Haz Login!</a>"
        return HttpResponse(respuesta)

    if request.method == "POST":
        valor = request.POST['valor']
        c = Contenido(clave=llave, valor=valor)  # Creo un nuevo modelo : parametro = nombreDeVariable
        c.save()
    try:
        # get lee de la base de datos
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de la llave es: " + contenido.valor
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = formulario
        else:
            respuesta = "No estas autenticado. <a href='/login'> Haz Login!</a>"
    return HttpResponse(respuesta)


def index(request): #esta vista se dara cuando nos pidan la /
    #1. Tener la lista de contenidos
    content_list = Contenido.objects.all()
    #2. Cargar la plantilla que esta en template
    template = loader.get_template('cms/index.html')
    #3. Ligar las variables de la plantilla a las variables de Python
    context = {
        'content_list': content_list #todos los contenidos de la lista
    }
    #4."Renderizar" es decir, ejecutar 3.
    return HttpResponse(template.render(context,request))

#Para la autentificacion del usuario
def loggedIn(request):
    if request.user.is_authenticated: #si el usuario esta autentificado
        respuesta = "Logged it as " + request.user.username
    else:
        respuesta = "No estas autenticado.<a href='/login'>Autenticate</a>"
    return HttpResponse(respuesta)

@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/cms/") #hazme un 302 de http a /cms

def imagen(request):
    template= loader.get_template('cms/plantilla.hmtl.html')
    context = {}
    return HttpResponse(template.render(context,request))