from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave = models.CharField(max_length=64) #Campo de texto limitado en tamaño. Pequeños para buscar de manera facil
    valor = models.TextField()

    ##PODEMOS AÑADIR FUNCIONES A LOS MODELOS
    def __str__(self): #funcion para hacer un print de Contenido
        return str(self.id) + ": " + self.clave + "--- " + self.valor


    def tiene_as(self):
        return ('a' in self.valor) #que devuelva True si el valor tiene una a




#Necesitamos la relacion entre Contenido y Comentario
#1 contenido puede tener N Comentarios.
#esto quiere decir, a cada comentario le tenemos que decir a que contenido le pertenece
#para ello la tabla que tendre que cambiar es la de comentarios

class Comentario(models.Model):
    #creamos una nueva tabla
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE) #para saber a que contenido corresponde
    #(contenido,parametros opcionales) en contenido va el identificador, models.CASCADE cuando se borra un contenido que se haga en cascada y se borre todos los comentarios
    titulo = models.CharField(max_length=128)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()

    #str para que se vea mucho mejor en la interfaz de administracion
    def __str__(self):
        return str(self.id) + ": " + self.titulo